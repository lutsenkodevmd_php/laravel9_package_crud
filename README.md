#Описание пакета
##  Менеджер управление данными в laravel . Универсальный CRUD

### Установка
Для того чтобы устоновить пакет нужно устоновить пакет авторизации
- composer require laravel/breeze --dev

и комманды набрать

- `php artisan breeze:install`
- `php artisan migrate`
- `npm install`
- `npm run dev`

И уже можно сам пакет устонавливать

- сomposer require lutsenkodevmd/crud

Добавить строчку  в app.php в папке conf
`\Lutsenkodevmd\Crud\Providers\CrudLutsenkodevmdProvider::class,`

### Функционал
Данный пакет позволяет быстро создавать формы для управления данными в базе данных (CRUD-систему). Также есть различные
поля котоыре могут быть в виде текста или файлов. 

Чтобы пользователь имел доступ в систему CRUD нужно чтобы поле is_crud было True или 1 