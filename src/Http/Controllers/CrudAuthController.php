<?php

namespace Lutsenkodevmd\Crud\Http\Controllers;

use Illuminate\Http\Request;

class CrudAuthController extends Controller
{
    /**
     * Выводим форму для входа в админку
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function login(){
        return view('lutsenkodevmdviews::login.loginform');
    }
    /**
     * Домашния странциа
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dashbord(){
        return view('lutsenkodevmdviews::crud.dashbord');

    }

}
