<?php

namespace Lutsenkodevmd\Crud\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use File;

class CrudController extends Controller
{

    /**
     * Проверяем могут эти пользователи октрывать что то
     * @param $user
     */
    protected function getAccess($user){
        if(!$user->is_crud){
            abort(403);
        }
    }

    /**
     * Получаем список всех кто нужен нам
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|int
     */
    public function listshow($name){
        $this->getAccess(Auth::user());
        //возврошаем какой массив нужно вернуть
        $datas=array();
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$name);
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }

        //получаем модель и все записи с таблицы
        $model = app($confData['model']);
        $dataDb=$model->all();
        $arrayLists=array();
        foreach ($confData['list'] as $key => $data){
            $arrayLists[]=$key;
        }
        // Заполняем даныне списка
        $datas['config']=$confData;
        $datas['datas']=$dataDb;
        $datas['arrayLists']=$arrayLists;
        $datas['name']=$name;
        //покаываем предстовление
        return view('lutsenkodevmdviews::crud.list',$datas);
    }


    /**
     * Выводим форму на создание записи
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|int
     */
    public function creatform($name){
        $this->getAccess(Auth::user());
        //возврошаем какой массив нужно вернуть
        $datas=array();
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$name);
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }
        // Заполняем даныне списка
        $datas['config']=$confData;
        $datas['name']=$name;
        //покаываем предстовление
        return view('lutsenkodevmdviews::crud.create',$datas);
    }

    /**
     * Создаем запись
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|int
     */
    public function storeform(Request $request,$name){
        $this->getAccess(Auth::user());
        //возврошаем какой массив нужно вернуть
        $datas=array();
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$request->input('nameconfig'));
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }
        // название кофигурации
        $nameconfig=$request->input('nameconfig');
        // массив для добавления
        foreach ($request->input($nameconfig) as $key=>$value){
            $valInsert="";
            if($key=="password"){
                $datas[$key] = Hash::make($value);
            }else{
                $datas[$key] = $value;
            }
        }
        //формируем массив
        $model = app($confData['model']);
        foreach ($confData['create'] as $key=>$value){

            // Если тип boolean и не выбрали значение  то по умолчанию ставим 0
            if(!array_key_exists($key, $datas)){
                if($value['type']=="boolean")
                    $datas[$key]=0;
            }

            // Если попали на тип файл то проверяем запрос
            if($value['type']=="file"){
                $files=$request->file($name);
                if(!is_null($files))

                    foreach($files as $keyFile=>$valueFile){

                        if($key==$keyFile){

                            $project_image = $request->file($name)[$keyFile];
                            $filename = time() . '.' . $project_image->getClientOriginalExtension();
                            $puth=$value['puth'];
                            $fileUploads='/uploads/'.$puth.'/'.$filename;
                            $destinationPath = public_path().'/uploads/'.$puth ;
                            $project_image->move($destinationPath,$filename);
                            $datas[$key]=$fileUploads;
                        }
                    }

            }

        }
        //dd($datas);
        //создаем запись
        $model::create(
            $datas
        );

        //возврошаемся на список
        return redirect()->route('lutsenkodevmdcrud.list',['name'=>$nameconfig]);
    }


    /**
     * @param $name
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|int
     */
    public function showedit($name,$id){
        $this->getAccess(Auth::user());
        //возврошаем какой массив нужно вернуть
        $datas=array();
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$name);
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }

        $model = app($confData['model']);
        $dataDb=$model::where('id',$id)->first();
        if(is_null($dataDb))
            abort(404);

        // Заполняем даныне списка
        $datas['config']=$confData;
        $datas['name']=$name;
        $datas['idrecord']=$id;
        $datas['dataDb']=$dataDb;
        //покаываем предстовление
        return view('lutsenkodevmdviews::crud.edit',$datas);
    }


    /**
     * Обновления данных
     * @param Request $request
     * @param $name
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|int
     */
    public function updateid(Request $request,$name,$id){
        $this->getAccess(Auth::user());
        //возврошаем какой массив нужно вернуть
        $datas=array();
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$name);
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }
        // название кофигурации
        $nameconfig=$name;
        // массив для добавления
        foreach ($request->input($nameconfig) as $key=>$value){
            if($key=="password"){
                $datas[$key] = Hash::make($value);
            }else{
                $datas[$key] = $value;
            }
        }
        //формируем массив
        $model = app($confData['model']);
        foreach ($confData['edit'] as $key=>$value){

            // Если тип boolean и не выбрали значение  то по умолчанию ставим 0
            if(!array_key_exists($key, $datas)){
                if($value['type']=="boolean")
                    $datas[$key]=0;
            }
            // Если попали на тип файл то проверяем запрос
            if($value['type']=="file"){
                $files=$request->file($name);
                if(!is_null($files))
                    foreach($files as $keyFile=>$valueFile){
                         if($key==$keyFile){
                           $project_image = $request->file($name)[$keyFile];
                            $filename = time() . '.' . $project_image->getClientOriginalExtension();
                            $puth=$value['puth'];
                            $fileUploads='/uploads/'.$puth.'/'.$filename;
                            $destinationPath = public_path().'/uploads/'.$puth ;
                            $project_image->move($destinationPath,$filename);
                            $datas[$key]=$fileUploads;

                         }
                    }
            }
        }
        //создаем запись
        $model::where('id', $id)->update(
            $datas
        );
        //возврошаемся на список
        return redirect()->route('lutsenkodevmdcrud.list',['name'=>$nameconfig]);


    }


    /**
     * Выводим компонент как страницу
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function component($name){
        $this->getAccess(Auth::user());
        return view('lutsenkodevmdviews::crud.showcomponent',['name'=>$name]);
    }

    public function delete($name,$id){
        $this->getAccess(Auth::user());
        // Подгружаем что должно вывести
        try{
            $confData=config('lutsenkodevmd_crud.'.$name);
        }catch (\Exception $exception){
            echo $exception->getMessage();
            return 1;
        }
        //получаем модель
        $model = app($confData['model']);
        // удоляем
        $model->where('id',$id)->delete();
        return redirect()->back();
    }

}
