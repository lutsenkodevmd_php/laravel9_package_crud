<?php

namespace Lutsenkodevmd\Crud\Providers;

use Illuminate\Support\ServiceProvider;

class CrudLutsenkodevmdProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'lutsenkodevmdviews');
        $this->publishes([
            __DIR__.'/../config/lutsenkodevmd_crud/menu.php' => config_path('lutsenkodevmd_crud/menu.php'),
        ]);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->publishes([
            __DIR__.'/../public' => public_path('lutsenkodevmd/crud'),
            __DIR__.'/../config/lutsenkodevmd_crud' => config_path('lutsenkodevmd_crud'),
            __DIR__.'/../resources/views/admincrud' => resource_path('views/admincrud'),

        ], 'public');

    }
}
