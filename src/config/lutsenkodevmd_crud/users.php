<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Пользователи
    |--------------------------------------------------------------------------
    |
    | Конфигуарция БД пользователя
    */


    // Путь к модели
    'model'=>'App\\Models\\User',




    /**
     * Страница списка
     */
    'listTitle'=>'Список пользователей',
    'list' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'name'=>[
            'title'=>'Ф.И.О.',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'emaik',
            'type'=>'text',
        ],
        'is_admin'=>[
            'title'=>'Админ',
            'type'=>'boolean',
        ],
    ],
    //какие действия при показе всех форм
    'list_event'=>['edit','delete'],

    // показываем форму для добавления
    'createTitle'=>'Создать пользователя',
    'create'=>[
        'name'=>[
            'title'=>'Ф.И.О.',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'email',
        ],
        'password'=>[
            'title'=>'Пароль',
            'type'=>'password',
        ],
        'avatar'=>[
            'title'=>'Аватарка',
            'type'=>'file',
            'puth'=>'useravatarr',
        ],
        'testtextarea'=>[
            'title'=>'Описание',
            'type'=>'textarea',
        ],
        'testdatatime'=>[
            'title'=>'Тестовая дата',
            'type'=>'date',
        ],
    ],

    // показываем форму для редактирования
    'editTitle'=>'Редактировать пользователя',
    'edit'=>[
        'name'=>[
            'title'=>'Ф.И.О.1',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'email',
        ],
        'avatar'=>[
            'title'=>'Пароль',
            'type'=>'file',
            'puth'=>'useravatarr',
        ],
        'testtextarea'=>[
            'title'=>'Описание',
            'type'=>'textarea',
        ],
        'testdatatime'=>[
            'title'=>'Тестовая дата',
            'type'=>'date',
        ],
    ]
];
