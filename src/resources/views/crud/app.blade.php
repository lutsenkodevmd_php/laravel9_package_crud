<html>
<head>
    <title></title>
    <link rel="stylesheet" href="{{  asset('/lutsenkodevmd/crud/css/mystyle.css') }}">


    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>
<body>


<div class="wrapper_head_back">
    <div class="left_part">
        AdminCRUD (0.3.4)
    </div>

    <div class="right_part">
        <form method="POST" action="{{ route('logout') }}">
            <div class="button"   onclick="event.preventDefault();
                                                this.closest('form').submit();">
@csrf
                Выход
            </div>
        </form>

    </div>
</div>


<div class="wrapper_body">
    <div class="left_part_body">

        <?php
            $menuConfs=config('lutsenkodevmd_crud.menu.components');
        ?>
        @foreach($menuConfs as $menuConf)
            <div class="menu_head" >
                <div class="button btncomp">
                    {{$menuConf['title']}}
                </div>
                <div class="menu_sun_menu">
                    @foreach($menuConf['submenu'] as $key=>$submenu)
                        @if($submenu['type']=="crud")
                            <a class="a_component" href="{{ route('lutsenkodevmdcrud.list',['name'=>$key])}}">
                                {{$submenu['title']}}
                            </a>
                        @endif
                        @if($submenu['type']=="pageсomponent")
                            <a class="a_component" href="{{ route('lutsenkodevmdcrud.component',['name'=>$key])}}">
                                {{$submenu['title']}}
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    <div class="content_part_body">
        @yield('content')
    </div>
</div>



<script src="{{asset('/lutsenkodevmd/crud/js/main.js')}}"></script>

@include('admincrud.include.footer_js')


</body>
</html>