@extends('lutsenkodevmdviews::crud.app')




@section('url_manager')
    <div class="url_menu" style="display: none">
        <a>Главная странциа</a>
        >
        Пользователи
    </div>
@endsection

@section('content')
    <div class="div_12_content">
        <div class="title_text">
            {{$config['editTitle']}}
        </div>
        <div class="data_show">

            <form method="post" enctype="multipart/form-data">
                @csrf()
                @foreach($config['edit'] as $key=>$value)
                    <div class="form_12_div">
                        <div class="title_form">
                            {{$value['title']}}
                        </div>
                        <div class="value_form">

                            <!-- -->
                            @if($value['type']=="text")
                                <input type="text"  name="{{$name.'['.$key.']'}}"

                                       class=" @if(isset($value['class_name']))
                                       @if($value['class_name'])
                                       {{$value['class_name']}}
                                       @endif
                                       @endif"
                                       @if(isset($value['readonly']))
                                       @if($value['readonly'])
                                       readonly="true"
                                       @endif
                                       @endif
                                       value="{{$dataDb->$key}}">
                            @endif
                            <!-- -->
                            @if($value['type']=="email")
                                <input type="email"
                                       class=" @if(isset($value['class_name']))
                                       @if($value['class_name'])
                                       {{$value['class_name']}}
                                       @endif
                                       @endif"

                                       name="{{$name.'['.$key.']'}}" value="{{$dataDb->$key}}">
                            @endif
                            <!-- -->
                            @if($value['type']=="textarea")
                                <textarea
                                        class=" @if(isset($value['class_name']))
                                        @if($value['class_name'])
                                        {{$value['class_name']}}
                                        @endif
                                        @endif"

                                        name="{{$name.'['.$key.']'}}" >{{$dataDb->$key}}</textarea>
                            @endif
                            <!-- -->
                                @if($value['type']=="date")
                                <input type="date"
                                       class=" @if(isset($value['class_name']))
                                       @if($value['class_name'])
                                       {{$value['class_name']}}
                                       @endif
                                       @endif"

                                       <?php
                                        $dataCarbon=\Carbon\Carbon::parse($dataDb->$key)->format('Y-m-d');
                                       ?>


                                       name="{{$name.'['.$key.']'}}" value="{{$dataCarbon}}">
                                @endif
                            <!-- -->
                            @if($value['type']=="boolean")
                                <input type="checkbox" name="{{$name.'['.$key.']'}}"
                                       @if($dataDb->$key)
                                               checked
                                               @endif
                                       value="1">
                            @endif
                            <!-- -->
                            @if($value['type']=="password")
                                <input type="password" name="{{$name.'['.$key.']'}}" value="{{$dataDb->$key}}">
                            @endif
                            <!-- -->
                            @if($value['type']=="component")

                                @include('admincrud.component.'.$value['namecomponent'],
                                    [   'name'=>$name,
                                        'value'=>$dataDb->$key,
                                        'id'=>$idrecord,
                                        '$key'=>$key])
                            @endif

                                <!-- -->
                            @if($value['type']=="file")
                                <input type="file" name="{{$name.'['.$key.']'}}"  value=""> <br>
                                @if(!is_null ( $dataDb->$key ) )
                                    <a href="{{ url($dataDb->$key)  }}" target="_blank">просмотреть файл</a>
                                @endif

                            @endif




                        </div>
                    </div>
                @endforeach
                <input type="hidden" value="{{$name}}" name="nameconfig">
                <div class="form_12_div">
                    <input type="submit">
                </div>

            </form>
        </div>

    </div>
@endsection