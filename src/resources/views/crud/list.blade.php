@extends('lutsenkodevmdviews::crud.app')



@section('url_manager')
    <div class="url_menu" style="display: none">
        <a>Главная странциа</a>
        >
        Пользователи
    </div>
@endsection

@section('content')
    <div class="div_12_content">
        <div class="title_text">
            {{$config['listTitle']}}
        </div>
        <div class="data_show">


            <a href="{{route('lutsenkodevmdcrud.creatform',['name'=>$name])}}" class="btn_plus">
                <img src="{{asset('/lutsenkodevmd/crud/images/png/plus48.png')}}" width="32" height="32" alt="редактировать">
                Новая запись
            </a>


            <table class="table_list">
                <thead>
                @foreach($config['list'] as $key=>$value)
                    <th>
                        {{$value['title']}}
                    </th>
                @endforeach
                <th>
                    Действие
                </th>
                </thead>
                <tbody>
                @foreach($datas as $data)
                    <tr>
                        @foreach($arrayLists as $arrayList)
                            <td>
                                {{$data->$arrayList}}
                            </td>
                        @endforeach

                        <td>
                            <a href="{{route('lutsenkodevmdcrud.showedit',['name'=>$name,'id'=>$data->id])}}" style="text-decoration: none">
                                <img src="{{asset('/lutsenkodevmd/crud/images/png/edit68.png')}}" width="32" height="32" alt="редактировать">
                            </a>
                            <a href="{{route('lutsenkodevmdcrud.delete',['name'=>$name,'id'=>$data->id])}}" style="text-decoration: none">
                                <img src="{{asset('/lutsenkodevmd/crud/images/png/delete48.png')}}"
                                     width="32" height="32" alt="удалить">
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection