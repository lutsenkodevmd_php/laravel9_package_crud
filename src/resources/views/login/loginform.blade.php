<html>
<head>
    <title></title>
    <link rel="stylesheet" href="{{ asset('/lutsenkodevmd/crud/css/login.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="form_login_wrapper">

    <div class="form_content">
        <form method="POST" action="{{ route('login') }}" class="formlogin">
            <div class="title_text">Вход</div>
            <div class="raw_input">email</div>
            <div class="raw_input_text">
                <input type="email" name="email" >
            </div>
            <div class="raw_input">Пароль</div>
            <div class="raw_input_text">
                <input type="password"  name="password">
            </div>
            <input type="submit" class="login_submit">Отправить</input>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

        </form>
    </div>
</div>
</body>
</html>