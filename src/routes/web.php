<?php


use Illuminate\Support\Facades\Route;
use Lutsenkodevmd\Crud\Http\Controllers\LutsenkodevmdCrudController;
use Lutsenkodevmd\Crud\Http\Controllers\CrudAuthController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use Lutsenkodevmd\Crud\Http\Controllers\CrudController;



Route::middleware(['web','auth'])->group(function () {
    Route::prefix('crudadmin')->group(function () {
        Route::get('/', [CrudAuthController::class,'dashbord']);
        //Главная страница
        Route::get('/dashboard', [CrudAuthController::class, 'dashbord'])
            ->name('lutsenkodevmdcrud.dashboard');
        //компонент
        Route::get('/component_{name}', [CrudController::class, 'component'])
            ->name('lutsenkodevmdcrud.component');
        // Список
        Route::get('/{name}', [CrudController::class, 'listshow'])
            ->name('lutsenkodevmdcrud.list');
        //Форма для создание новой записи
        Route::get('/{name}/create', [CrudController::class, 'creatform'])
            ->name('lutsenkodevmdcrud.creatform');
        //Форма отпарвки
        Route::post('/{name}/create', [CrudController::class, 'storeform'])
            ->name('lutsenkodevmdcrud.storeform');
        //Форма на редактирование
        Route::get('/{name}/show/{id}', [CrudController::class, 'showedit'])
            ->name('lutsenkodevmdcrud.showedit');
        //Удаление записи
        Route::get('/{name}/delete/{id}', [CrudController::class, 'delete'])
            ->name('lutsenkodevmdcrud.delete');
        //Форма на редактирование
        Route::post('/{name}/show/{id}', [CrudController::class, 'updateid'])
            ->name('lutsenkodevmdcrud.showedit');
    });


});
/*

Route::prefix('crudadmin')->group(function () {
    Route::get('/', [CrudAuthController::class,'dashbord']);
    //Главная страница
    Route::get('/dashboard', [CrudAuthController::class, 'dashbord'])
        ->name('lutsenkodevmdcrud.dashboard');
    //компонент
    Route::get('/component_{name}', [CrudController::class, 'component'])
        ->name('lutsenkodevmdcrud.component');
    // Список
    Route::get('/{name}', [CrudController::class, 'listshow'])
        ->name('lutsenkodevmdcrud.list');
    //Форма для создание новой записи
    Route::get('/{name}/create', [CrudController::class, 'creatform'])
        ->name('lutsenkodevmdcrud.creatform');
    //Форма отпарвки
    Route::post('/{name}/create', [CrudController::class, 'storeform'])
        ->name('lutsenkodevmdcrud.storeform');
    //Форма на редактирование
    Route::get('/{name}/show/{id}', [CrudController::class, 'showedit'])
        ->name('lutsenkodevmdcrud.showedit');
    //Форма на редактирование
    Route::post('/{name}/show/{id}', [CrudController::class, 'updateid'])
        ->name('lutsenkodevmdcrud.showedit');
});
*/

/**
 * Точка входа в админку
 */
Route::middleware('web')->group(function () {
    Route::get('/login', [CrudAuthController::class,'login']);
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->name('login');
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});
